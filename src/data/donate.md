
#### ![BTC](https://georgi.karapetrov.top/images/coin/BTC.jpg) Bitcoin

    bc1qkg9lxmtfw7tx7erdfuggphw50z5nuxtnrd6leq

![BitCoinQR](https://georgi.karapetrov.top/images/coin/BitCoinQR.jpg)

#### ![XMR](https://georgi.karapetrov.top/images/coin/XMR.jpg) Monero

    4BB6hC5oq1fhzSFmgTWdaPKGQeZRh4hNLTMGT9PNKa1agiBGWEDmVL66TUzrvoWvmP3jLRbbVGRrSUXrkndXgFhQQN2QCnR

![MoneroQR](https://georgi.karapetrov.top/images/coin/MoneroQR.jpg)